import numpy as np
import more_itertools as mit
import networkx as nx
import itertools


def absdet(m):
    return abs(det(m))
    
def det(m):
    l = len(m)
    if l == 1:
        return m[0][0]
    if l == 2:
        return det2(m[0],m[1])
    if l == 3:
        return det3(m[0],m[1],m[2])
    if l == 4:
        return det4(m[0],m[1],m[2],m[3])
    if l == 5:
        return det5(m[0],m[1],m[2],m[3],m[4])
    if l > 5:
        return int(round(np.linalg.det(m)))
    
def det2(a,b):
    return a[0]*b[1] - a[1]*b[0]
    
def det3(a,b,c):
    return a[0]*b[1]*c[2] - a[0]*b[2]*c[1] - a[1]*b[0]*c[2] + a[1]*b[2]*c[0] + a[2]*b[0]*c[1] - a[2]*b[1]*c[0]

def det4(a,b,c,d):
    return a[0]*b[1]*c[2]*d[3]-a[0]*b[1]*c[3]*d[2]-a[0]*b[2]*c[1]*d[3]+a[0]*b[2]*c[3]*d[1]+a[0]*b[3]*c[1]*d[2]-a[0]*b[3]*c[2]*d[1]-a[1]*b[0]*c[2]*d[3]+a[1]*b[0]*c[3]*d[2]+a[1]*b[2]*c[0]*d[3]-a[1]*b[2]*c[3]*d[0]-a[1]*b[3]*c[0]*d[2]+a[1]*b[3]*c[2]*d[0]+a[2]*b[0]*c[1]*d[3]-a[2]*b[0]*c[3]*d[1]-a[2]*b[1]*c[0]*d[3]+a[2]*b[1]*c[3]*d[0]+a[2]*b[3]*c[0]*d[1]-a[2]*b[3]*c[1]*d[0]-a[3]*b[0]*c[1]*d[2]+a[3]*b[0]*c[2]*d[1]+a[3]*b[1]*c[0]*d[2]-a[3]*b[1]*c[2]*d[0]-a[3]*b[2]*c[0]*d[1]+a[3]*b[2]*c[1]*d[0]
    
def det5(a,b,c,d,e):
    return a[0]*b[1]*c[2]*d[3]*e[4]-a[0]*b[1]*c[2]*d[4]*e[3]-a[0]*b[1]*c[3]*d[2]*e[4]+a[0]*b[1]*c[3]*d[4]*e[2]+a[0]*b[1]*c[4]*d[2]*e[3]-a[0]*b[1]*c[4]*d[3]*e[2]-a[0]*b[2]*c[1]*d[3]*e[4]+a[0]*b[2]*c[1]*d[4]*e[3]+a[0]*b[2]*c[3]*d[1]*e[4]-a[0]*b[2]*c[3]*d[4]*e[1]-a[0]*b[2]*c[4]*d[1]*e[3]+a[0]*b[2]*c[4]*d[3]*e[1]+a[0]*b[3]*c[1]*d[2]*e[4]-a[0]*b[3]*c[1]*d[4]*e[2]-a[0]*b[3]*c[2]*d[1]*e[4]+a[0]*b[3]*c[2]*d[4]*e[1]+a[0]*b[3]*c[4]*d[1]*e[2]-a[0]*b[3]*c[4]*d[2]*e[1]-a[0]*b[4]*c[1]*d[2]*e[3]+a[0]*b[4]*c[1]*d[3]*e[2]+a[0]*b[4]*c[2]*d[1]*e[3]-a[0]*b[4]*c[2]*d[3]*e[1]-a[0]*b[4]*c[3]*d[1]*e[2]+a[0]*b[4]*c[3]*d[2]*e[1]-a[1]*b[0]*c[2]*d[3]*e[4]+a[1]*b[0]*c[2]*d[4]*e[3]+a[1]*b[0]*c[3]*d[2]*e[4]-a[1]*b[0]*c[3]*d[4]*e[2]-a[1]*b[0]*c[4]*d[2]*e[3]+a[1]*b[0]*c[4]*d[3]*e[2]+a[1]*b[2]*c[0]*d[3]*e[4]-a[1]*b[2]*c[0]*d[4]*e[3]-a[1]*b[2]*c[3]*d[0]*e[4]+a[1]*b[2]*c[3]*d[4]*e[0]+a[1]*b[2]*c[4]*d[0]*e[3]-a[1]*b[2]*c[4]*d[3]*e[0]-a[1]*b[3]*c[0]*d[2]*e[4]+a[1]*b[3]*c[0]*d[4]*e[2]+a[1]*b[3]*c[2]*d[0]*e[4]-a[1]*b[3]*c[2]*d[4]*e[0]-a[1]*b[3]*c[4]*d[0]*e[2]+a[1]*b[3]*c[4]*d[2]*e[0]+a[1]*b[4]*c[0]*d[2]*e[3]-a[1]*b[4]*c[0]*d[3]*e[2]-a[1]*b[4]*c[2]*d[0]*e[3]+a[1]*b[4]*c[2]*d[3]*e[0]+a[1]*b[4]*c[3]*d[0]*e[2]-a[1]*b[4]*c[3]*d[2]*e[0]+a[2]*b[0]*c[1]*d[3]*e[4]-a[2]*b[0]*c[1]*d[4]*e[3]-a[2]*b[0]*c[3]*d[1]*e[4]+a[2]*b[0]*c[3]*d[4]*e[1]+a[2]*b[0]*c[4]*d[1]*e[3]-a[2]*b[0]*c[4]*d[3]*e[1]-a[2]*b[1]*c[0]*d[3]*e[4]+a[2]*b[1]*c[0]*d[4]*e[3]+a[2]*b[1]*c[3]*d[0]*e[4]-a[2]*b[1]*c[3]*d[4]*e[0]-a[2]*b[1]*c[4]*d[0]*e[3]+a[2]*b[1]*c[4]*d[3]*e[0]+a[2]*b[3]*c[0]*d[1]*e[4]-a[2]*b[3]*c[0]*d[4]*e[1]-a[2]*b[3]*c[1]*d[0]*e[4]+a[2]*b[3]*c[1]*d[4]*e[0]+a[2]*b[3]*c[4]*d[0]*e[1]-a[2]*b[3]*c[4]*d[1]*e[0]-a[2]*b[4]*c[0]*d[1]*e[3]+a[2]*b[4]*c[0]*d[3]*e[1]+a[2]*b[4]*c[1]*d[0]*e[3]-a[2]*b[4]*c[1]*d[3]*e[0]-a[2]*b[4]*c[3]*d[0]*e[1]+a[2]*b[4]*c[3]*d[1]*e[0]-a[3]*b[0]*c[1]*d[2]*e[4]+a[3]*b[0]*c[1]*d[4]*e[2]+a[3]*b[0]*c[2]*d[1]*e[4]-a[3]*b[0]*c[2]*d[4]*e[1]-a[3]*b[0]*c[4]*d[1]*e[2]+a[3]*b[0]*c[4]*d[2]*e[1]+a[3]*b[1]*c[0]*d[2]*e[4]-a[3]*b[1]*c[0]*d[4]*e[2]-a[3]*b[1]*c[2]*d[0]*e[4]+a[3]*b[1]*c[2]*d[4]*e[0]+a[3]*b[1]*c[4]*d[0]*e[2]-a[3]*b[1]*c[4]*d[2]*e[0]-a[3]*b[2]*c[0]*d[1]*e[4]+a[3]*b[2]*c[0]*d[4]*e[1]+a[3]*b[2]*c[1]*d[0]*e[4]-a[3]*b[2]*c[1]*d[4]*e[0]-a[3]*b[2]*c[4]*d[0]*e[1]+a[3]*b[2]*c[4]*d[1]*e[0]+a[3]*b[4]*c[0]*d[1]*e[2]-a[3]*b[4]*c[0]*d[2]*e[1]-a[3]*b[4]*c[1]*d[0]*e[2]+a[3]*b[4]*c[1]*d[2]*e[0]+a[3]*b[4]*c[2]*d[0]*e[1]-a[3]*b[4]*c[2]*d[1]*e[0]+a[4]*b[0]*c[1]*d[2]*e[3]-a[4]*b[0]*c[1]*d[3]*e[2]-a[4]*b[0]*c[2]*d[1]*e[3]+a[4]*b[0]*c[2]*d[3]*e[1]+a[4]*b[0]*c[3]*d[1]*e[2]-a[4]*b[0]*c[3]*d[2]*e[1]-a[4]*b[1]*c[0]*d[2]*e[3]+a[4]*b[1]*c[0]*d[3]*e[2]+a[4]*b[1]*c[2]*d[0]*e[3]-a[4]*b[1]*c[2]*d[3]*e[0]-a[4]*b[1]*c[3]*d[0]*e[2]+a[4]*b[1]*c[3]*d[2]*e[0]+a[4]*b[2]*c[0]*d[1]*e[3]-a[4]*b[2]*c[0]*d[3]*e[1]-a[4]*b[2]*c[1]*d[0]*e[3]+a[4]*b[2]*c[1]*d[3]*e[0]+a[4]*b[2]*c[3]*d[0]*e[1]-a[4]*b[2]*c[3]*d[1]*e[0]-a[4]*b[3]*c[0]*d[1]*e[2]+a[4]*b[3]*c[0]*d[2]*e[1]+a[4]*b[3]*c[1]*d[0]*e[2]-a[4]*b[3]*c[1]*d[2]*e[0]-a[4]*b[3]*c[2]*d[0]*e[1]+a[4]*b[3]*c[2]*d[1]*e[0]

def algorithm_u(ns, m):
    def visit(n, a):
        ps = [[] for i in range(m)]
        for j in range(n):
            ps[a[j + 1]].append(ns[j])
        return ps

    def f(mu, nu, sigma, n, a):
        if mu == 2:
            yield visit(n, a)
        else:
            for v in f(mu - 1, nu - 1, (mu + sigma) % 2, n, a):
                yield v
        if nu == mu + 1:
            a[mu] = mu - 1
            yield visit(n, a)
            while a[nu] > 0:
                a[nu] = a[nu] - 1
                yield visit(n, a)
        elif nu > mu + 1:
            if (mu + sigma) % 2 == 1:
                a[nu - 1] = mu - 1
            else:
                a[mu] = mu - 1
            if (a[nu] + sigma) % 2 == 1:
                for v in b(mu, nu - 1, 0, n, a):
                    yield v
            else:
                for v in f(mu, nu - 1, 0, n, a):
                    yield v
            while a[nu] > 0:
                a[nu] = a[nu] - 1
                if (a[nu] + sigma) % 2 == 1:
                    for v in b(mu, nu - 1, 0, n, a):
                        yield v
                else:
                    for v in f(mu, nu - 1, 0, n, a):
                        yield v

    def b(mu, nu, sigma, n, a):
        if nu == mu + 1:
            while a[nu] < mu - 1:
                yield visit(n, a)
                a[nu] = a[nu] + 1
            yield visit(n, a)
            a[mu] = 0
        elif nu > mu + 1:
            if (a[nu] + sigma) % 2 == 1:
                for v in f(mu, nu - 1, 0, n, a):
                    yield v
            else:
                for v in b(mu, nu - 1, 0, n, a):
                    yield v
            while a[nu] < mu - 1:
                a[nu] = a[nu] + 1
                if (a[nu] + sigma) % 2 == 1:
                    for v in f(mu, nu - 1, 0, n, a):
                        yield v
                else:
                    for v in b(mu, nu - 1, 0, n, a):
                        yield v
            if (mu + sigma) % 2 == 1:
                a[nu - 1] = 0
            else:
                a[mu] = 0
        if mu == 2:
            yield visit(n, a)
        else:
            for v in b(mu - 1, nu - 1, (mu + sigma) % 2, n, a):
                yield v

    n = len(ns)
    a = [0] * (n + 1)
    for j in range(1, m + 1):
        a[n - m + j] = j - 1
    return f(m, n, 0, n, a)

def det_histogram(mtx, d):
    n = len(mtx[0])
    hist = [0]*(d+1)
    for tup in itertools.combinations(mtx, n):
        tup_det = absdet(tup)
        if tup_det <= d:
            hist[tup_det] += 1
        else:
            return False
    return hist


def matroid_connectivity(m):
    n = len(m[0])
    for i in range(len(m)):
        for part in algorithm_u(m, 2):
            if min(len(part[0]), len(part[1])) >= i:
                if np.linalg.matrix_rank(part[0])+np.linalg.matrix_rank(part[1]) <= n + i - 1:
                    '''print(part[0])
                    print('----------')
                    print(part[1])'''
                    return i
 
def vertical_connectivity(m):
    n = len(m[0])
    for i in range(n):
        for part in algorithm_u(m, 2):
            if min(np.linalg.matrix_rank(part[0]), np.linalg.matrix_rank(part[1])) >= i:
                if np.linalg.matrix_rank(part[0])+np.linalg.matrix_rank(part[1]) <= n + i - 1:
                    '''print(part[0])
                    print('----------')
                    print(part[1])'''
                    return i     
    return n

def row_gcd(rows):
    m = len(rows)
    if m == 0:
        return 0
    n = len(rows[0])
    dets = [absdet([tuple(r[t[i]] for i in range(m)) for r in rows]) for t in itertools.combinations(range(n),m)]
    return gcd(dets)

def simple_minor(m):    
    for i in range(len(m.groundset())):
        for tup in itertools.combinations(m.groundset(), i):
            newm = m.delete(set(tup))
            if len(newm.circuits()) > 0:
                if min([len(c) for c in newm.circuits()]) > 2:
                    return newm
            else:
                return newm

def find_gcd_minor(mat):
    for i in range(len(mat.groundset())):
        for ele_tuple in itertools.combinations(mat.groundset(), i):
            gcdm=row_gcd([mat.representation_vectors()[i] for i in ele_tuple])
            if gcdm == 2:
                return i


  

def find_regular_minor(mat):
    minor_count = [0,0,0]
    regular_minors = []
    regular_tuples = []
    for i in range(len(mat.groundset())):
        for ele_tuple in itertools.combinations(mat.groundset(), i):
            for part in algorithm_u(ele_tuple,2):
                if det_histogram(list(Matrix(mat.representation_vectors()[k] for k in mat.groundset()-set(part[1]))),1) or row_gcd([mat.representation_vectors()[i] for i in part[0]])==2:
                    regular_tuples.append([ele_tuple, len(part[0]), len(part[1])])
                    rm = Matroid(mat.delete(part[1]).contract(part[0]), regular=True)
                    regular_minors.append(rm)
                    
                if det_histogram(list(Matrix(mat.representation_vectors()[k] for k in mat.groundset()-set(part[0]))),1) or row_gcd([mat.representation_vectors()[i] for i in part[1]])==2:
                    regular_tuples.append([ele_tuple, len(part[1]), len(part[0])])
                    rm = Matroid(mat.delete(part[0]).contract(part[1]), regular=True)
                    regular_minors.append(rm)    
            if i > 1:
                part = (ele_tuple, set())
                if det_histogram(list(Matrix(mat.representation_vectors()[k] for k in mat.groundset()-set(part[1]))),1) or row_gcd([mat.representation_vectors()[i] for i in part[0]])==2:
                    regular_tuples.append([ele_tuple, len(part[0]), len(part[1])])
                    rm = Matroid(mat.delete(part[1]).contract(part[0]), regular=True)
                    regular_minors.append(rm)
                    
                if det_histogram(list(Matrix(mat.representation_vectors()[k] for k in mat.groundset()-set(part[0]))),1) or row_gcd([mat.representation_vectors()[i] for i in part[1]])==2:
                    regular_tuples.append([ele_tuple, len(part[1]), len(part[0])])
                    rm = Matroid(mat.delete(part[0]).contract(part[1]), regular=True)
                    regular_minors.append(rm)  
                
        if len(regular_minors) > 0:
            break
    max_rank = 0
    max_elements = 0
    for minor in regular_minors:
        max_rank = max(max_rank, minor.rank())
        max_elements = max(max_elements, len(minor.groundset()))
    print('\\begin{itemize}')
    max_regular_minors = []
    for i in range(len(regular_minors)):
        minor = regular_minors[i]
        tup = regular_tuples[i]
        
        if len(minor.groundset()) == max_elements:
            if not any([minor.is_isomorphic(mnr) for mnr in max_regular_minors]):
                
                if minor.is_graphic():
                    minor_count[0]=1
                else:
                    if minor.dual().is_graphic():
                        minor_count[1]=1
                    else:
                        minor_count[2]=1
                max_regular_minors.append(minor)
                if tup[1] > 0 and tup[2] > 0:
                    opstr = str(tup[1])+' contraction(s) and '+str(tup[2])+' deletion(s)'
                if tup[1] == 0:
                    opstr = str(tup[2])+' deletion(s)'
                if tup[2] == 0:
                    opstr = str(tup[1])+' contraction(s)'
                if minor.is_graphic():
                    print('\\item Graphic', minor, 'after', opstr)
                else:
                    if minor.dual().is_graphic():
                        print('\\item Cographic', minor, 'after', opstr)
    print('\\end{itemize}')
    return minor_count