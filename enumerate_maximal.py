from datetime import datetime
import itertools
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter
import copy

def absdet(m):
    return abs(det(m))
    
def det(m):
    l = len(m)

    if l == 1:
        return m[0][0]
    if l == 2:
        return det2(m[0],m[1])
    if l == 3:
        return det3(m[0],m[1],m[2])
    if l == 4:
        if (0,0,0,0) in m:
            return 0
        if (1,0,0,0) in m:
            i = m.index((1,0,0,0))
            m = [r[1:] for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])
        if (-1,0,0,0) in m:
            i = m.index((-1,0,0,0))
            m = [r[1:] for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])
        if (0,0,0,1) in m:
            i = m.index((0,0,0,1))
            m = [r[:-1] for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])
        if (0,0,0,-1) in m:
            i = m.index((0,0,0,-1))
            m = [r[:-1] for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])
        if (0,0,1,0) in m:
            i = m.index((0,0,1,0))
            m = [r[:2]+r[3:] for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])
        if (0,0,-1,0) in m:
            i = m.index((0,0,-1,0))
            m = [r[:2]+r[3:] for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])
        if (0,1,0,0) in m:
            i = m.index((0,1,0,0))
            m = [r[:1]+r[2:] for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])
        if (0,-1,0,0) in m:
            i = m.index((0,-1,0,0))
            m = [r[:1]+r[2:] for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])
        if (1,1,0,0) in m:
            i = m.index((1,1,0,0))
            m = [(r[0]-r[1],r[2],r[3]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])       
        if (1,-1,0,0) in m:
            i = m.index((1,-1,0,0))
            m = [(r[0]+r[1],r[2],r[3]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2]) 
        if (1,0,1,0) in m:
            i = m.index((1,0,1,0))
            m = [(r[0]-r[2],r[1],r[3]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])     
        if (1,0,-1,0) in m:
            i = m.index((1,0,-1,0))
            m = [(r[0]+r[2],r[1],r[3]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2]) 
        if (0,1,1,0) in m:
            i = m.index((0,1,1,0))
            m = [(r[0],r[1]-r[2],r[3]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])   
        if (0,1,-1,0) in m:
            i = m.index((0,1,-1,0))
            m = [(r[0],r[1]+r[2],r[3]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])   
        if (1,0,0,1) in m:
            i = m.index((1,0,0,1))
            m = [(r[0]-r[3],r[1],r[2]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])   
        if (1,0,0,-1) in m:
            i = m.index((1,0,0,-1))
            m = [(r[0]+r[3],r[1],r[2]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2]) 
        if (0,0,1,1) in m:
            i = m.index((0,0,1,1))
            m = [(r[0],r[1],r[2]-r[3]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2]) 
        if (0,0,1,-1) in m:
            i = m.index((0,0,1,-1))
            m = [(r[0],r[1],r[2]+r[3]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2]) 
        if (0,1,0,1) in m:
            i = m.index((0,1,0,1))
            m = [(r[0],r[1]-r[3],r[2]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])  
        if (0,1,0,-1) in m:
            i = m.index((0,1,0,-1))
            m = [(r[0],r[1]+r[3],r[2]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])  
        if (1,1,1,0) in m:
            i = m.index((1,1,1,0))
            m = [(r[1]-r[0],r[2]-r[0],r[3]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])  
        if (1,1,-1,0) in m:
            i = m.index((1,1,-1,0))
            m = [(r[1]-r[0],r[2]+r[0],r[3]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])  
        if (1,-1,1,0) in m:
            i = m.index((1,-1,1,0))
            m = [(r[1]+r[0],r[2]-r[0],r[3]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])  
        if (1,-1,-1,0) in m:
            i = m.index((1,-1,-1,0))
            m = [(r[1]+r[0],r[2]+r[0],r[3]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])  
        if (1,1,0,1) in m:
            i = m.index((1,1,0,1))
            m = [(r[1]-r[0],r[2],r[3]-r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])  
        if (1,1,0,-1) in m:
            i = m.index((1,1,0,-1))
            m = [(r[1]-r[0],r[2],r[3]+r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])  
        if (1,-1,0,1) in m:
            i = m.index((1,-1,0,1))
            m = [(r[1]+r[0],r[2],r[3]-r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])  
        if (1,-1,0,-1) in m:
            i = m.index((1,-1,0,-1))
            m = [(r[1]+r[0],r[2],r[3]+r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2]) 
        if (1,0,1,1) in m:
            i = m.index((1,0,1,1))
            m = [(r[1],r[2]-r[0],r[3]-r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])  
        if (1,0,1,-1) in m:
            i = m.index((1,0,1,-1))
            m = [(r[1],r[2]-r[0],r[3]+r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])  
        if (1,0,-1,1) in m:
            i = m.index((1,0,-1,1))
            m = [(r[1],r[2]+r[0],r[3]-r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2]) 
        if (1,0,-1,-1) in m:
            i = m.index((1,0,-1,-1))
            m = [(r[1],r[2]+r[0],r[3]+r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2]) 
        if (0,1,1,1) in m:
            i = m.index((0,1,1,1))
            m = [(r[0],r[2]-r[1],r[3]-r[1]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2]) 
        if (0,1,1,-1) in m:
            i = m.index((0,1,1,-1))
            m = [(r[0],r[2]-r[1],r[3]+r[1]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2]) 
        if (0,1,-1,1) in m:
            i = m.index((0,1,-1,1))
            m = [(r[0],r[2]+r[1],r[3]-r[1]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2]) 
        if (0,1,-1,-1) in m:
            i = m.index((0,1,-1,-1))
            m = [(r[0],r[2]+r[1],r[3]+r[1]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2]) 
        if (1,1,1,1) in m:
            i = m.index((1,1,1,1))
            m = [(r[1]-r[0],r[2]-r[0],r[3]-r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2]) 
        if (1,1,1,-1) in m:
            i = m.index((1,1,1,-1))
            m = [(r[1]-r[0],r[2]-r[0],r[3]+r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])        
        if (1,1,-1,1) in m:
            i = m.index((1,1,-1,1))
            m = [(r[1]-r[0],r[2]+r[0],r[3]-r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])       
        if (1,1,-1,-1) in m:
            i = m.index((1,1,-1,-1))
            m = [(r[1]-r[0],r[2]+r[0],r[3]+r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])    
        if (1,-1,1,1) in m:
            i = m.index((1,-1,1,1))
            m = [(r[1]+r[0],r[2]-r[0],r[3]-r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])      
        if (1,-1,1,-1) in m:
            i = m.index((1,-1,1,-1))
            m = [(r[1]+r[0],r[2]-r[0],r[3]+r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])
        if (1,-1,-1,1) in m:
            i = m.index((1,-1,-1,1))
            m = [(r[1]+r[0],r[2]+r[0],r[3]-r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])   
        if (1,-1,-1,-1) in m:
            i = m.index((1,-1,-1,-1))
            m = [(r[1]+r[0],r[2]+r[0],r[3]+r[0]) for r in m[:i]+m[i+1:]]
            return det3(m[0],m[1],m[2])   
        '''for x in range(4):
            if max(abs(k) for k in m[x]) <= 1:
                for y in range(4):
                    if m[x][y] != 0:
                        coefs = [m[x][y]*r for r in m[x]]
                        m = [tuple(coefs[l]*m[x][y]+r[l] for l in range(4)) for r in m]
                        return det3(m[0],m[1],m[2])'''
        return det4(m[0],m[1],m[2],m[3])
    if l == 5:
        if (0,0,0,0,0) in m:
            return 0
        if (1,0,0,0,0) in m:
            i = m.index((1,0,0,0,0))
            m = [r[1:] for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,0,0,-1) in m:
            i = m.index((0,0,0,0,-1))
            m = [r[:-1] for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,0,1,0) in m:
            i = m.index((0,0,0,1,0))
            m = [r[:3]+r[4:] for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,1,0,0) in m:
            i = m.index((0,0,1,0,0))
            m = [r[:2]+r[3:] for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,1,0,0,0) in m:
            i = m.index((0,1,0,0,0))
            m = [r[:1]+r[2:] for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,0,0,-2) in m:
            i = m.index((0,0,0,0,-2))
            m = [r[:-1] for r in m[:i]+m[i+1:]]
            return 2*det(m)  
        if (1,1,0,0,0) in m:
            i = m.index((1,1,0,0,0))
            m = [(r[1]-r[0],r[2],r[3],r[4]) for r in m[:i]+m[i+1:]]
            return det(m)  
        if (1,-1,0,0,0) in m:
            i = m.index((1,-1,0,0,0))
            m = [(r[1]+r[0],r[2],r[3],r[4]) for r in m[:i]+m[i+1:]]
            return det(m)     
        if (1,0,1,0,0) in m:
            i = m.index((1,0,1,0,0))
            m = [(r[1],r[2]-r[0],r[3],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,0,-1,0,0) in m:
            i = m.index((1,0,-1,0,0))
            m = [(r[1],r[2]+r[0],r[3],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,0,0,1,0) in m:
            i = m.index((1,0,0,1,0))
            m = [(r[1],r[2],r[3]-r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,0,0,-1,0) in m:
            i = m.index((1,0,0,-1,0))
            m = [(r[1],r[2],r[3]+r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,1,1,0,0) in m:
            i = m.index((0,1,1,0,0))
            m = [(r[0],r[2]-r[1],r[3],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,1,-1,0,0) in m:
            i = m.index((0,1,-1,0,0))
            m = [(r[0],r[2]+r[1],r[3],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,1,0,1,0) in m:
            i = m.index((0,1,0,1,0))
            m = [(r[0],r[2],r[3]-r[1],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,1,0,-1,0) in m:
            i = m.index((0,1,0,-1,0))
            m = [(r[0],r[2],r[3]+r[1],r[4]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,1,1,0) in m:
            i = m.index((0,0,1,1,0))
            m = [(r[0],r[1],r[3]-r[2],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,0,1,-1,0) in m:
            i = m.index((0,0,1,-1,0))
            m = [(r[0],r[1],r[3]+r[2],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,0,0,-1,-1) in m:
            i = m.index((0,0,0,-1,-1))
            m = [(r[0],r[1],r[2],r[4]-r[3]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,0,-1,0,-1) in m:
            i = m.index((0,0,-1,0,-1))
            m = [(r[0],r[1],r[3],r[4]-r[2]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,-1,0,0,-1) in m:
            i = m.index((0,-1,0,0,-1))
            m = [(r[0],r[2],r[3],r[4]-r[1]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,0,0,0,1) in m:
            i = m.index((1,0,0,0,1))
            m = [(r[1],r[2],r[3],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,1,1,0,0) in m:
            i = m.index((1,1,1,0,0))
            m = [(r[1]-r[0],r[2]-r[0],r[3],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,1,-1,0,0) in m:
            i = m.index((1,1,-1,0,0))
            m = [(r[1]-r[0],r[2]+r[0],r[3],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,-1,1,0,0) in m:
            i = m.index((1,-1,1,0,0))
            m = [(r[1]+r[0],r[2]-r[0],r[3],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,-1,-1,0,0) in m:
            i = m.index((1,-1,-1,0,0))
            m = [(r[1]+r[0],r[2]+r[0],r[3],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,1,0,1,0) in m:
            i = m.index((1,1,0,1,0))
            m = [(r[1]-r[0],r[2],r[3]-r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,1,0,-1,0) in m:
            i = m.index((1,1,0,-1,0))
            m = [(r[1]-r[0],r[2],r[3]+r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,-1,0,1,0) in m:
            i = m.index((1,-1,0,1,0))
            m = [(r[1]+r[0],r[2],r[3]-r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,-1,0,-1,0) in m:
            i = m.index((1,-1,0,-1,0))
            m = [(r[1]+r[0],r[2],r[3]+r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,0,1,1,0) in m:
            i = m.index((1,0,1,1,0))
            m = [(r[1],r[2]-r[0],r[3]-r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,0,1,-1,0) in m:
            i = m.index((1,0,1,-1,0))
            m = [(r[1],r[2]-r[0],r[3]+r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,0,-1,1,0) in m:
            i = m.index((1,0,-1,1,0))
            m = [(r[1],r[2]+r[0],r[3]-r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,0,-1,-1,0) in m:
            i = m.index((1,0,-1,-1,0))
            m = [(r[1],r[2]+r[0],r[3]+r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,1,0,0,1) in m:
            i = m.index((1,1,0,0,1))
            m = [(r[1]-r[0],r[2],r[3],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,1,1,1,0) in m:
            i = m.index((0,1,1,1,0))
            m = [(r[0],r[2]-r[1],r[3]-r[1],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,1,1,-1,0) in m:
            i = m.index((0,1,1,-1,0))
            m = [(r[0],r[2]-r[1],r[3]+r[1],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,1,-1,1,0) in m:
            i = m.index((0,1,-1,1,0))
            m = [(r[0],r[2]+r[1],r[3]-r[1],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,1,-1,-1,0) in m:
            i = m.index((0,1,-1,-1,0))
            m = [(r[0],r[2]+r[1],r[3]+r[1],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,0,-1,-1,-1) in m:
            i = m.index((0,0,-1,-1,-1))
            m = [(r[0],r[1],r[3]-r[2],r[4]-r[2]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,-1,0,-1,-1) in m:
            i = m.index((0,-1,0,-1,-1))
            m = [(r[0],r[2],r[3]-r[1],r[4]-r[1]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,-1,-1,0,-1) in m:
            i = m.index((0,-1,-1,0,-1))
            m = [(r[0],r[2]-r[1],r[3],r[4]-r[1]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,0,1,0,1) in m:
            i = m.index((1,0,1,0,1))
            m = [(r[1],r[2]-r[0],r[3],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,0,0,1,1) in m:
            i = m.index((1,0,0,1,1))
            m = [(r[1],r[2],r[3]-r[0],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,1,1,1,0) in m:
            i = m.index((1,1,1,1,0))
            m = [(r[1]-r[0],r[2]-r[0],r[3]-r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m)   
        if (1,1,1,-1,0) in m:
            i = m.index((1,1,1,-1,0))
            m = [(r[1]-r[0],r[2]-r[0],r[3]+r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,1,-1,1,0) in m:
            i = m.index((1,1,-1,1,0))
            m = [(r[1]-r[0],r[2]+r[0],r[3]-r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,1,-1,-1,0) in m:
            i = m.index((1,1,-1,-1,0))
            m = [(r[1]-r[0],r[2]+r[0],r[3]+r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,-1,1,-1,0) in m:
            i = m.index((1,-1,1,-1,0))
            m = [(r[1]+r[0],r[2]-r[0],r[3]+r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,-1,-1,1,0) in m:
            i = m.index((1,-1,-1,1,0))
            m = [(r[1]+r[0],r[2]+r[0],r[3]-r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,-1,-1,-1,0) in m:
            i = m.index((1,-1,-1,-1,0))
            m = [(r[1]+r[0],r[2]+r[0],r[3]+r[0],r[4]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,1,1,0,1) in m:
            i = m.index((1,1,1,0,1))
            m = [(r[1]-r[0],r[2]-r[0],r[3],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,1,0,1,1) in m:
            i = m.index((1,1,0,1,1))
            m = [(r[1]-r[0],r[2],r[3]-r[0],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (1,0,1,1,1) in m:
            i = m.index((1,0,1,1,1))
            m = [(r[1],r[2]-r[0],r[3]-r[0],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,-1,-1,-1,-1) in m:
            i = m.index((0,-1,-1,-1,-1))
            m = [(r[0],r[2]-r[1],r[3]-r[1],r[4]-r[1]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (0,0,0,-2,-2) in m:
            i = m.index((0,0,0,-2,-2))
            m = [(r[0],r[1],r[2],r[4]-r[3]) for r in m[:i]+m[i+1:]]
            return 2*det(m) 
        if (0,0,-2,0,-2) in m:
            i = m.index((0,0,-2,0,-2))
            m = [(r[0],r[1],r[4]-r[2],r[3]) for r in m[:i]+m[i+1:]]
            return 2*det(m) 
        if (2,0,0,0,2) in m:
            i = m.index((2,0,0,0,2))
            m = [(r[1],r[2],r[3],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return 2*det(m) 
        if (2,2,0,0,2) in m:
            i = m.index((2,2,0,0,2))
            m = [(r[1]-r[0],r[2],r[3],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return 2*det(m) 
        if (2,0,2,0,2) in m:
            i = m.index((2,0,2,0,2))
            m = [(r[1],r[2]-r[0],r[3],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return 2*det(m) 
        if (2,0,0,2,2) in m:
            i = m.index((2,0,0,2,2))
            m = [(r[1],r[2],r[3]-r[0],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return 2*det(m) 
        if (0,0,-2,-2,-2) in m:
            i = m.index((0,0,-2,-2,-2))
            m = [(r[0],r[1],r[3]-r[2],r[4]-r[2]) for r in m[:i]+m[i+1:]]
            return 2*det(m) 
        if (0,-2,0,-2,-2) in m:
            i = m.index((0,-2,0,-2,-2))
            m = [(r[0],r[2],r[3]-r[1],r[4]-r[1]) for r in m[:i]+m[i+1:]]
            return 2*det(m) 
        if (0,-2,-2,0,-2) in m:
            i = m.index((0,-2,-2,0,-2))
            m = [(r[0],r[2]-r[1],r[3],r[4]-r[1]) for r in m[:i]+m[i+1:]]
            return 2*det(m) 
        if (2,2,2,0,2) in m:
            i = m.index((2,2,2,0,2))
            m = [(r[1]-r[0],r[2]-r[0],r[3],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return 2*det(m) 
        if (2,2,0,2,2) in m:
            i = m.index((2,2,0,2,2))
            m = [(r[1]-r[0],r[2],r[3]-r[0],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return 2*det(m)
        if (2,0,2,2,2) in m:
            i = m.index((2,0,2,2,2))
            m = [(r[1],r[2]-r[0],r[3]-r[0],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return 2*det(m)
        if (0,-2,-2,-2,-2) in m:
            i = m.index((0,-2,-2,-2,-2))
            m = [(r[0],r[2]-r[1],r[3]-r[1],r[4]-r[1]) for r in m[:i]+m[i+1:]]
            return 2*det(m) 
        if (1,1,1,1,1) in m:
            i = m.index((1,1,1,1,1))
            m = [(r[1]-r[0],r[2]-r[0],r[3]-r[0],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return det(m) 
        if (2,2,2,2,2) in m:
            i = m.index((2,2,2,2,2))
            m = [(r[1]-r[0],r[2]-r[0],r[3]-r[0],r[4]-r[0]) for r in m[:i]+m[i+1:]]
            return 2*det(m) 
        if (1,0,0,0,2) in m:
            i = m.index((1,0,0,0,2))
            m = [(r[1],r[2],r[3],r[4]-2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (-1,0,0,0,-2) in m:
            i = m.index((-1,0,0,0,-2))
            m = [(r[1],r[2],r[3],r[4]-2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,0,0,0,-2) in m:
            i = m.index((1,0,0,0,-2))
            m = [(r[1],r[2],r[3],r[4]+2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,1,0,0,2) in m:
            i = m.index((0,1,0,0,2))
            m = [(r[0],r[2],r[3],r[4]-2*r[1]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,-1,0,0,-2) in m:
            i = m.index((0,-1,0,0,-2))
            m = [(r[0],r[2],r[3],r[4]-2*r[1]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,1,0,0,-2) in m:
            i = m.index((0,1,0,0,-2))
            m = [(r[0],r[2],r[3],r[4]+2*r[1]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,1,0,2) in m:
            i = m.index((0,0,1,0,2))
            m = [(r[0],r[1],r[3],r[4]-2*r[2]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,-1,0,-2) in m:
            i = m.index((0,0,-1,0,-2))
            m = [(r[0],r[1],r[3],r[4]-2*r[2]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,1,0,-2) in m:
            i = m.index((0,0,1,0,-2))
            m = [(r[0],r[1],r[3],r[4]+2*r[2]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,0,1,2) in m:
            i = m.index((0,0,0,1,2))
            m = [(r[0],r[1],r[2],r[4]-2*r[3]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,0,1,-2) in m:
            i = m.index((0,0,0,1,-2))
            m = [(r[0],r[1],r[2],r[4]+2*r[3]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,0,-1,-2) in m:
            i = m.index((0,0,0,-1,-2))
            m = [(r[0],r[1],r[2],r[4]-2*r[3]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,1,0,0,2) in m:
            i = m.index((1,1,0,0,2))
            m = [(r[1]-r[0],r[2],r[3],r[4]-2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,-1,0,0,2) in m:
            i = m.index((1,-1,0,0,2))
            m = [(r[1]+r[0],r[2],r[3],r[4]-2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,-1,0,0,-2) in m:
            i = m.index((1,-1,0,0,-2))
            m = [(r[1]+r[0],r[2],r[3],r[4]+2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,1,0,0,-2) in m:
            i = m.index((1,1,0,0,-2))
            m = [(r[1]-r[0],r[2],r[3],r[4]+2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (-1,-1,0,0,-2) in m:
            i = m.index((-1,-1,0,0,-2))
            m = [(r[1]-r[0],r[2],r[3],r[4]-2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,0,1,0,2) in m:
            i = m.index((1,0,1,0,2))
            m = [(r[1],r[2]-r[0],r[3],r[4]-2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,0,-1,0,2) in m:
            i = m.index((1,0,-1,0,2))
            m = [(r[1],r[2]+r[0],r[3],r[4]-2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,0,-1,0,-2) in m:
            i = m.index((1,0,-1,0,-2))
            m = [(r[1],r[2]+r[0],r[3],r[4]+2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (-1,0,-1,0,-2) in m:
            i = m.index((-1,0,-1,0,-2))
            m = [(r[1],r[2]-r[0],r[3],r[4]-2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,0,1,0,-2) in m:
            i = m.index((1,0,1,0,-2))
            m = [(r[1],r[2]-r[0],r[3],r[4]+2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,0,0,1,2) in m:
            i = m.index((1,0,0,1,2))
            m = [(r[1],r[2],r[3]-r[0],r[4]-2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,0,0,-1,2) in m:
            i = m.index((1,0,0,-1,2))
            m = [(r[1],r[2],r[3]+r[0],r[4]-2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,0,0,1,-2) in m:
            i = m.index((1,0,0,1,-2))
            m = [(r[1],r[2],r[3]-r[0],r[4]+2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,0,0,-1,-2) in m:
            i = m.index((1,0,0,-1,-2))
            m = [(r[1],r[2],r[3]+r[0],r[4]+2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,1,1,0,2) in m:
            i = m.index((0,1,1,0,2))
            m = [(r[0],r[2]-r[1],r[3],r[4]-2*r[1]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,-1,-1,0,-2) in m:
            i = m.index((0,-1,-1,0,-2))
            m = [(r[0],r[2]-r[1],r[3],r[4]-2*r[1]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,1,-1,0,2) in m:
            i = m.index((0,1,-1,0,2))
            m = [(r[0],r[2]+r[1],r[3],r[4]-2*r[1]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,1,-1,0,-2) in m:
            i = m.index((0,1,-1,0,-2))
            m = [(r[0],r[2]+r[1],r[3],r[4]+2*r[1]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,1,1,0,-2) in m:
            i = m.index((0,1,1,0,-2))
            m = [(r[0],r[2]-r[1],r[3],r[4]+2*r[1]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,1,0,1,2) in m:
            i = m.index((0,1,0,1,2))
            m = [(r[0],r[2],r[3]-r[1],r[4]-2*r[1]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,1,0,-1,2) in m:
            i = m.index((0,1,0,-1,2))
            m = [(r[0],r[2],r[3]+r[1],r[4]-2*r[1]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,-1,0,-1,-2) in m:
            i = m.index((0,-1,0,-1,-2))
            m = [(r[0],r[2],r[3]-r[1],r[4]-2*r[1]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,1,0,1,-2) in m:
            i = m.index((0,1,0,1,-2))
            m = [(r[0],r[2],r[3]-r[1],r[4]+2*r[1]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,1,0,-1,-2) in m:
            i = m.index((0,1,0,-1,-2))
            m = [(r[0],r[2],r[3]+r[1],r[4]+2*r[1]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,1,1,2) in m:
            i = m.index((0,0,1,1,2))
            m = [(r[0],r[1],r[3]-r[2],r[4]-2*r[2]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,1,-1,2) in m:
            i = m.index((0,0,1,-1,2))
            m = [(r[0],r[1],r[3]+r[2],r[4]-2*r[2]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,1,1,-2) in m:
            i = m.index((0,0,1,1,-2))
            m = [(r[0],r[1],r[3]-r[2],r[4]+2*r[2]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,1,-1,-2) in m:
            i = m.index((0,0,1,-1,-2))
            m = [(r[0],r[1],r[3]+r[2],r[4]+2*r[2]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (0,0,-1,-1,-2) in m:
            i = m.index((0,0,-1,-1,-2))
            m = [(r[0],r[1],r[3]-r[2],r[4]-2*r[2]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,1,1,0,2) in m:
            i = m.index((1,1,1,0,2))
            m = [(r[1]-r[0],r[2]-r[0],r[3],r[4]-2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        if (1,1,1,0,-2) in m:
            i = m.index((1,1,1,0,-2))
            m = [(r[1]-r[0],r[2]-r[0],r[3],r[4]+2*r[0]) for r in m[:i]+m[i+1:]]
            return det(m)
        return det5(m[0],m[1],m[2],m[3],m[4])
    return int(round(np.linalg.det(m)))
def det2(a,b):
    return a[0]*b[1] - a[1]*b[0]
    
def det3(a,b,c):
    return a[0]*b[1]*c[2] - a[0]*b[2]*c[1] - a[1]*b[0]*c[2] + a[1]*b[2]*c[0] + a[2]*b[0]*c[1] - a[2]*b[1]*c[0]

def det4(a,b,c,d):
    return a[0]*b[1]*c[2]*d[3]-a[0]*b[1]*c[3]*d[2]-a[0]*b[2]*c[1]*d[3]+a[0]*b[2]*c[3]*d[1]+a[0]*b[3]*c[1]*d[2]-a[0]*b[3]*c[2]*d[1]-a[1]*b[0]*c[2]*d[3]+a[1]*b[0]*c[3]*d[2]+a[1]*b[2]*c[0]*d[3]-a[1]*b[2]*c[3]*d[0]-a[1]*b[3]*c[0]*d[2]+a[1]*b[3]*c[2]*d[0]+a[2]*b[0]*c[1]*d[3]-a[2]*b[0]*c[3]*d[1]-a[2]*b[1]*c[0]*d[3]+a[2]*b[1]*c[3]*d[0]+a[2]*b[3]*c[0]*d[1]-a[2]*b[3]*c[1]*d[0]-a[3]*b[0]*c[1]*d[2]+a[3]*b[0]*c[2]*d[1]+a[3]*b[1]*c[0]*d[2]-a[3]*b[1]*c[2]*d[0]-a[3]*b[2]*c[0]*d[1]+a[3]*b[2]*c[1]*d[0]
    
def det5(a,b,c,d,e):
    return a[0]*b[1]*c[2]*d[3]*e[4]-a[0]*b[1]*c[2]*d[4]*e[3]-a[0]*b[1]*c[3]*d[2]*e[4]+a[0]*b[1]*c[3]*d[4]*e[2]+a[0]*b[1]*c[4]*d[2]*e[3]-a[0]*b[1]*c[4]*d[3]*e[2]-a[0]*b[2]*c[1]*d[3]*e[4]+a[0]*b[2]*c[1]*d[4]*e[3]+a[0]*b[2]*c[3]*d[1]*e[4]-a[0]*b[2]*c[3]*d[4]*e[1]-a[0]*b[2]*c[4]*d[1]*e[3]+a[0]*b[2]*c[4]*d[3]*e[1]+a[0]*b[3]*c[1]*d[2]*e[4]-a[0]*b[3]*c[1]*d[4]*e[2]-a[0]*b[3]*c[2]*d[1]*e[4]+a[0]*b[3]*c[2]*d[4]*e[1]+a[0]*b[3]*c[4]*d[1]*e[2]-a[0]*b[3]*c[4]*d[2]*e[1]-a[0]*b[4]*c[1]*d[2]*e[3]+a[0]*b[4]*c[1]*d[3]*e[2]+a[0]*b[4]*c[2]*d[1]*e[3]-a[0]*b[4]*c[2]*d[3]*e[1]-a[0]*b[4]*c[3]*d[1]*e[2]+a[0]*b[4]*c[3]*d[2]*e[1]-a[1]*b[0]*c[2]*d[3]*e[4]+a[1]*b[0]*c[2]*d[4]*e[3]+a[1]*b[0]*c[3]*d[2]*e[4]-a[1]*b[0]*c[3]*d[4]*e[2]-a[1]*b[0]*c[4]*d[2]*e[3]+a[1]*b[0]*c[4]*d[3]*e[2]+a[1]*b[2]*c[0]*d[3]*e[4]-a[1]*b[2]*c[0]*d[4]*e[3]-a[1]*b[2]*c[3]*d[0]*e[4]+a[1]*b[2]*c[3]*d[4]*e[0]+a[1]*b[2]*c[4]*d[0]*e[3]-a[1]*b[2]*c[4]*d[3]*e[0]-a[1]*b[3]*c[0]*d[2]*e[4]+a[1]*b[3]*c[0]*d[4]*e[2]+a[1]*b[3]*c[2]*d[0]*e[4]-a[1]*b[3]*c[2]*d[4]*e[0]-a[1]*b[3]*c[4]*d[0]*e[2]+a[1]*b[3]*c[4]*d[2]*e[0]+a[1]*b[4]*c[0]*d[2]*e[3]-a[1]*b[4]*c[0]*d[3]*e[2]-a[1]*b[4]*c[2]*d[0]*e[3]+a[1]*b[4]*c[2]*d[3]*e[0]+a[1]*b[4]*c[3]*d[0]*e[2]-a[1]*b[4]*c[3]*d[2]*e[0]+a[2]*b[0]*c[1]*d[3]*e[4]-a[2]*b[0]*c[1]*d[4]*e[3]-a[2]*b[0]*c[3]*d[1]*e[4]+a[2]*b[0]*c[3]*d[4]*e[1]+a[2]*b[0]*c[4]*d[1]*e[3]-a[2]*b[0]*c[4]*d[3]*e[1]-a[2]*b[1]*c[0]*d[3]*e[4]+a[2]*b[1]*c[0]*d[4]*e[3]+a[2]*b[1]*c[3]*d[0]*e[4]-a[2]*b[1]*c[3]*d[4]*e[0]-a[2]*b[1]*c[4]*d[0]*e[3]+a[2]*b[1]*c[4]*d[3]*e[0]+a[2]*b[3]*c[0]*d[1]*e[4]-a[2]*b[3]*c[0]*d[4]*e[1]-a[2]*b[3]*c[1]*d[0]*e[4]+a[2]*b[3]*c[1]*d[4]*e[0]+a[2]*b[3]*c[4]*d[0]*e[1]-a[2]*b[3]*c[4]*d[1]*e[0]-a[2]*b[4]*c[0]*d[1]*e[3]+a[2]*b[4]*c[0]*d[3]*e[1]+a[2]*b[4]*c[1]*d[0]*e[3]-a[2]*b[4]*c[1]*d[3]*e[0]-a[2]*b[4]*c[3]*d[0]*e[1]+a[2]*b[4]*c[3]*d[1]*e[0]-a[3]*b[0]*c[1]*d[2]*e[4]+a[3]*b[0]*c[1]*d[4]*e[2]+a[3]*b[0]*c[2]*d[1]*e[4]-a[3]*b[0]*c[2]*d[4]*e[1]-a[3]*b[0]*c[4]*d[1]*e[2]+a[3]*b[0]*c[4]*d[2]*e[1]+a[3]*b[1]*c[0]*d[2]*e[4]-a[3]*b[1]*c[0]*d[4]*e[2]-a[3]*b[1]*c[2]*d[0]*e[4]+a[3]*b[1]*c[2]*d[4]*e[0]+a[3]*b[1]*c[4]*d[0]*e[2]-a[3]*b[1]*c[4]*d[2]*e[0]-a[3]*b[2]*c[0]*d[1]*e[4]+a[3]*b[2]*c[0]*d[4]*e[1]+a[3]*b[2]*c[1]*d[0]*e[4]-a[3]*b[2]*c[1]*d[4]*e[0]-a[3]*b[2]*c[4]*d[0]*e[1]+a[3]*b[2]*c[4]*d[1]*e[0]+a[3]*b[4]*c[0]*d[1]*e[2]-a[3]*b[4]*c[0]*d[2]*e[1]-a[3]*b[4]*c[1]*d[0]*e[2]+a[3]*b[4]*c[1]*d[2]*e[0]+a[3]*b[4]*c[2]*d[0]*e[1]-a[3]*b[4]*c[2]*d[1]*e[0]+a[4]*b[0]*c[1]*d[2]*e[3]-a[4]*b[0]*c[1]*d[3]*e[2]-a[4]*b[0]*c[2]*d[1]*e[3]+a[4]*b[0]*c[2]*d[3]*e[1]+a[4]*b[0]*c[3]*d[1]*e[2]-a[4]*b[0]*c[3]*d[2]*e[1]-a[4]*b[1]*c[0]*d[2]*e[3]+a[4]*b[1]*c[0]*d[3]*e[2]+a[4]*b[1]*c[2]*d[0]*e[3]-a[4]*b[1]*c[2]*d[3]*e[0]-a[4]*b[1]*c[3]*d[0]*e[2]+a[4]*b[1]*c[3]*d[2]*e[0]+a[4]*b[2]*c[0]*d[1]*e[3]-a[4]*b[2]*c[0]*d[3]*e[1]-a[4]*b[2]*c[1]*d[0]*e[3]+a[4]*b[2]*c[1]*d[3]*e[0]+a[4]*b[2]*c[3]*d[0]*e[1]-a[4]*b[2]*c[3]*d[1]*e[0]-a[4]*b[3]*c[0]*d[1]*e[2]+a[4]*b[3]*c[0]*d[2]*e[1]+a[4]*b[3]*c[1]*d[0]*e[2]-a[4]*b[3]*c[1]*d[2]*e[0]-a[4]*b[3]*c[2]*d[0]*e[1]+a[4]*b[3]*c[2]*d[1]*e[0]

def permutations_with_signs(iterable, r=None):
    pool = tuple(iterable)
    n = len(pool)
    s = 0
    r = n if r is None else r
    if r > n:
        return
    indices = list(range(n))
    cycles = list(range(n,n-r,-1))
    yield (s,tuple(pool[i] for i in indices[:r]))
    while n:
        for i in reversed(range(r)):
            cycles[i] -= 1
            if cycles[i] == 0:
                indices[i:] = indices[i+1:] + indices[i:i+1]
                cycles[i] = n-i
                s += n - 1 - i
            else:
                j = cycles[i]
                indices[i], indices[-j] = indices[-j], indices[i]
                s += 1
                yield (s % 2,tuple(pool[i] for i in indices[:r]))
                break
        else:
            return

def compute_newly_incompatible(n, d, pts_list, contained_pts, untouched_pts, x):
    incompatible_pts = set()
    for a in untouched_pts:
        for index_tuple in itertools.combinations(contained_pts,n-2):
            mtx = [pts_list[i] for i in (a,x)+index_tuple]
            if absdet(mtx) > d:
                incompatible_pts.add(a)
                break
    return incompatible_pts

def enumerate_polytopes(n, d, pts_list, all_pts, enumeration, contained_pts, candidate_pts, h, p, v, l):
    global histograms
    global vistograms
    hist, v_hist = extended_histogram(contained_pts, n, d, pts_list, h.copy(), p, copy.deepcopy(v))

    complete = True
    if hist:
        v_hist_check = set()
        cistogram = Counter([tuple(h) for h in v_hist.values()])
        for c in cistogram:
            v_hist_check.add(c+(cistogram[c],))
        if tuple(hist) not in histograms or frozenset(v_hist_check) not in vistograms or len(v_hist) <= l+2:
            histograms.append(tuple(hist))
            vistograms.add(frozenset(v_hist_check))
            copy_v = dict()
            for x in candidate_pts:
                copy_v[x] = dict()
                for i in v_hist:
                    copy_v[x][i] = v_hist[i]
                if enumerate_polytopes(n, d, pts_list, all_pts, enumeration, contained_pts | {x}, candidate_pts - {x}, hist.copy(), x, copy_v[x], l):
                    complete = False
                
                else:
                    candidate_pts = candidate_pts - {x}
                #print(v == copy_v[x])
            if complete:
                l = len(enumeration)
                enumeration[tuple(hist)] = [pts_list[i] for i in contained_pts]
                if len(enumeration) > l:
                    print(len(enumeration))
    return hist
        
    '''incomp = compute_newly_incompatible(n, d, pts_list, contained_pts, all_pts - (forbidden_pts | contained_pts), p)
    new_candidate_pts = candidate_pts - incomp
    if len(new_candidate_pts) == 0:
        if len(ignored_pts - (forbidden_pts | incomp)) == 0:
            output_polytope(n, d, pts_list, enumeration, contained_pts | {p})
    else:
        new_p = new_candidate_pts.pop()
        enumerate_polytopes(n, d, pts_list, all_pts, enumeration, contained_pts | {p}, ignored_pts, forbidden_pts | incomp, new_candidate_pts,  new_p)
    new_incomp = incomp - ignored_pts
    if len(new_incomp) > 0:
        extranew_p = new_incomp.pop()
        enumerate_polytopes(n, d, pts_list, all_pts, enumeration, contained_pts, ignored_pts | {p}, forbidden_pts, candidate_pts-{extranew_p}, extranew_p)'''
 
def extended_histogram(polytope, n, d, pts_list, histo, p, v_histo):
    #v_histo[p] = [0]*(d+1)
    for index_tuple in itertools.combinations(polytope-{p},n-1):      
        mtx = [pts_list[i] for i in index_tuple+(p,)]
        mtx_det = absdet(mtx)
        if mtx_det <= d:
        #print(index_tuple, mtx, mtx_det)
            histo[mtx_det] += 1
            for i in index_tuple+(p,):
                if i not in v_histo:
                    v_histo[i] = [0]*(d+1)
                v_histo[i][mtx_det] += 1
        else:
            return False, False
    histo[-1] = len(polytope)
    return histo, copy.deepcopy(v_histo)

def histogram(polytope, n, d, pts_list):
    histo = [0]*(d+2)
    d_hist = dict()
    for i in polytope:
        d_hist[i] = [0]*(d+1)
    for index_tuple in itertools.combinations(polytope,n):     
        mtx = [pts_list[i] for i in index_tuple]
        mtx_det = absdet(mtx)
        
        if mtx_det <= d:
            for i in index_tuple:
                d_hist[i][mtx_det] += 1
            histo[mtx_det] += 1        
        else:
            return False, False
    histo[-1] = len(polytope)
    return histo, copy.deepcopy(d_hist)
 
def output_polytope(n, d, pts_list, enumeration, polytope):
    histogram = [0 for _ in range(d+2)]
    # print("-----------")
    # for x in polytope:
        # print(pts_list[x])
    # print("-----------")
    for index_tuple in itertools.combinations(polytope,n):
        mtx = [pts_list[i] for i in index_tuple]
        mtx_det = absdet(mtx)
        #print(index_tuple, mtx, mtx_det)
        histogram[mtx_det] += 1
    histogram[-1] = len(polytope)
    # print(histogram[0:d+1], histogram[-1]) 
    l = len(enumeration)                   
    enumeration[tuple(histogram)] = [pts_list[i] for i in polytope]
    if len(enumeration) > l:
        print(len(enumeration))

def weakly_increasing(n,d,m=0):
    if n==0:
        yield ()
        return
    for i in range(m,d+1):
        for x in weakly_increasing(n-1,d,i):
            yield (i,)+x

def lattice_points(q,d):
    n = len(q)
    for i in range(d):
        yield tuple(-i*q[k] % d for k in range(n))+(i,)
        
def enumerate_integral_translates(t,d,after_first_nonzero_position=False):
    if len(t) == 0:
        yield ()
        return
    t_rest = t[1:]
    t_0 = t[0]
    if t_0 == 0:
        for x in enumerate_integral_translates(t_rest,d,True):
            yield (t_0 + d,) + x
    for x in enumerate_integral_translates(t_rest,d,after_first_nonzero_position or t_0 > 0):
        yield (t_0,) + x
    if after_first_nonzero_position:
        for x in enumerate_integral_translates(t_rest,d,True):
            yield (t_0 - d,) + x

def get_points_in_octahedron(n,d, pts_list):
    p_oct = []
    for i in range(len(pts_list)):
        if sum(abs(k) for k in pts_list[i]) <= d:
            p_oct.append(i)
    return p_oct

def enumerate_all_instances(n,d):
    global histograms
    enumeration = {}
    for x in weakly_increasing(n-1,int(d/2)):
        hnf_row = x + (d,)
        # print("hnf row")
        # print(hnf_row)
        l = []
        for y in lattice_points(x,d):
            for z in enumerate_integral_translates(y,d):
                l.append(z)
        l.pop(l.index((tuple(0 for _ in range(n)))))
        # print("+++++++++++++++++++++++++++")
        # for t in l:
            # print(t)
        # print("+++++++++++++++++++++++++++")
        pts_list = l
        included_points = set(get_points_in_octahedron(n,d,pts_list))
        # for x in included_points:
            # print(x)
        # print("+++++++++++++++++++++++++++")
        all_pts = set(range(len(pts_list)))

        included_points = set(get_points_in_octahedron(n,d,pts_list))
        
        pts_list_new = [tuple(int((pt[i]/d) + hnf_row[i]*(pt[-1])/d) for i in range(n-1))+(pt[-1],) for pt in pts_list]
        candidate_pts = all_pts-included_points
        p = list(included_points)[0]
        l = len(included_points)
        start_hist, start_vist = histogram(included_points-{p},n,d,pts_list_new)
        #histograms = list()
        enumerate_polytopes(n,d,pts_list_new,all_pts,enumeration,included_points, candidate_pts, start_hist, p, copy.deepcopy(start_vist), l)
    return enumeration
    

def test_weakly_inncreasing_increasing():
    for x in weakly_increasing(4,5):
        print(x)

def test_permutations_with_signs():
    s = "abcde"
    n = len(s)
    signs = ['+','-']
    det = ""
    for p in permutations(tuple(range(n))):
        det += signs[p[0]] + '*'.join(s[i]+'['+str(p[1][i])+']' for i in range(n))
    print(det)

def test_enumerate_integral_translates():
    for x in enumerate_integral_translates((0,1,1),3):
        print(x)

def test_all_hnf_rows():
    n = 4
    d = 4
    for x in weakly_increasing(n-1,int(d/2)):
        print("hnf row")
        print(x+(d,))
        l = []
        for y in lattice_points(x,d):
            for z in enumerate_integral_translates(y,d):
                l.append(z)
        print("--")
        print(len(l))
        for t in l:
            print(t)
        print("--")

def plot_enumeration(n,d):
    enumeration = enumerate_all_instances(n,d)
    # x axis values
    x = list(range(d+1))
    
    m = max(max(histogram[:-1]) for histogram in enumeration)
    
    for histogram in enumeration:
        # plotting the points
        plt.plot(x, list(histogram[:-1]), linewidth = 2,
                 marker='o', markersize=3)
     
    # setting x and y axis range
    plt.ylim(0,m)
    plt.xlim(0,d)
    
    # naming the x axis
    plt.xlabel('subdeterminant absolute values')
    # naming the y axis
    plt.ylabel('frequency')
    
    title_string = f"n_equals_{n}_and_delta_equals_{d}"
    # giving a title to my graph
    plt.title(title_string)
    
    # function to save file
    plt.savefig(f"{title_string}.png", dpi=300)
    plt.clf()

if __name__ == "__main__":
    # n_d_pairs = [(2,1),(2,2),(2,3),(2,4),(2,5),(3,1),(3,2),(3,3),(3,4),(3,5)]
    # for (n,d) in n_d_pairs:
        # plot_enumeration(n,d)
    
    # print("============")
    startTime = datetime.now()
    n = 2
    d = 2
    global histograms
    global vistograms
    vistograms = set()
    histograms  = list()
    enumeration = enumerate_all_instances(n,d)
    for instance in enumeration:
        print(instance[0:d+1], instance[-1])
        '''for x in enumeration[instance]:
            print(x)'''
        print("--")
    print("Duration: " + str(datetime.now() - startTime))